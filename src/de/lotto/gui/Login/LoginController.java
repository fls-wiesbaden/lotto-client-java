package de.lotto.gui.Login;


import de.lotto.gui.LottoMain.LottoMainView;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class LoginController {

    @FXML
    private GridPane loginMainPane;

    @FXML
    private TextField username;

    @FXML
    private TextField portNumber;

    @FXML
    private TextField hostName;

    public void onClickStart(){

        boolean hasError = false;
        int port = 3000;

        if(username.getText().isEmpty()){
            hasError = true;
        }

        if(hostName.getText().isEmpty()){
            hasError = true;
        }

        try{
            if(!portNumber.getText().isEmpty()){
                port = Integer.parseUnsignedInt(portNumber.getText());
            }
        }catch(NumberFormatException nfe){
            hasError = true;
        }

        if(!hasError){
           Stage stage = (Stage) loginMainPane.getScene().getWindow();
           stage.close();
           new LottoMainView(port, hostName.getText(), username.getText());
        }

    }
}
