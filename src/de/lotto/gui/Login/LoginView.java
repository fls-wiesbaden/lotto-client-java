package de.lotto.gui.Login;

import de.lotto.gui.ErrorStackPrint;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class LoginView extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
        primaryStage.setTitle("Lotto - Login");
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(ErrorStackPrint.class.getResourceAsStream("assets/logo/people.png")));
        primaryStage.setScene(new Scene(root, 400, 450));
        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
        });
        primaryStage.show();
    }

}
