package de.lotto.gui.LottoMain.balance;

import de.lotto.Client.WrapVariables;
import de.lotto.database.MySqlConnection;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BalanceController {

    private Scene lottoScene;

    @FXML
    private VBox balanceStage;

    @FXML
    private Label balanceNumber;

    @FXML
    private TextField addMoneyInput;

    @FXML
    public void initialize(){
        this.getBalance();
        this.addMoneyInput.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,7}([\\,]\\d{0,2})?")) {
                this.addMoneyInput.setText(oldValue);
            }
        }));
    }

    public void setLottoScene(Scene lottoScene) {
        this.lottoScene = lottoScene;
    }

    public void onPayMoney(ActionEvent e){

        String input = this.addMoneyInput.getText().replace(',', '.');
        int money = Integer.parseInt(input);

        try{
            Thread.sleep(500);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        getBalance();
    }

    public void onRefreshBalance() {
        getBalance();
    }

    public void getBalance() {
        MySqlConnection mySqlConnection = new MySqlConnection();

        try{
            PreparedStatement statement = mySqlConnection.connect().prepareStatement("SELECT balance FROM user WHERE user_id=?");
            statement.setInt(1, WrapVariables.user.getUser_id());
            ResultSet rs = statement.executeQuery();
            if(rs.first()){
                this.balanceNumber.setText(rs.getBigDecimal("balance").toString());
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            mySqlConnection.disconnect();
        }
    }

    public void goBack(){
        Stage stage = (Stage) this.balanceStage.getScene().getWindow();
        stage.setScene(this.lottoScene);
    }

}
