package de.lotto.gui.LottoMain.balance;

import de.lotto.gui.LottoMain.history.HistoryController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class BalanceView {

    private Scene lottoScene;

    public BalanceView(Scene lottoScene){
        this.lottoScene = lottoScene;
    }

    public Scene getScene(){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("balance.fxml"));
            Parent root = loader.load();
            BalanceController controller = loader.getController();
            controller.setLottoScene(this.lottoScene);
            Scene mainScene = new Scene(root, 500, 600);

            /*
             * @comment Note that perhaps the css file isnt correct and has to be adjusted
             * */
            mainScene.getStylesheets().add(getClass().getResource("../../assets/css/lotto.min.css").toExternalForm());

            return mainScene;
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }
}
