package de.lotto.gui.LottoMain;

import de.lotto.Client.Client;
import de.lotto.database.MySqlConnection;
import de.lotto.gui.ErrorStackPrint;
import de.lotto.models.User;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class LottoMainView {

    private Client client;
    private final String username;

    public LottoMainView(int port, String hostname, String username){
        this.username = username;
        createClientConnection(port, hostname);
    }

    public void createClientConnection(int port, String hostname) {
        try{
            this.client = new Client(port, hostname, username);
            this.client.verbinden();

            try {
                this.openLottoMainWindow();
            } catch (Exception er) {
                ErrorStackPrint errorOpenGui = new ErrorStackPrint(er, "Gui Error", "Failed to open the Lotto Window", "Sorry but an error occured. Please try to restart the Application or report this issue.");
                Alert errorOPenGuiAlert = errorOpenGui.createErrorWindow();
                errorOPenGuiAlert.showAndWait();
                Platform.exit();
            }

        }catch(Exception e){
            ErrorStackPrint errorStackPrint = new ErrorStackPrint(e, "Connection error", "Failed trying to connect to the server", "The connection to the server failed please try again and check your data");
            Alert alert = errorStackPrint.createErrorWindow();
            alert.showAndWait();
            //Platform.exit();

            /*
             * TODO remove after finish debugging not good for production
             */

            try {
                this.openLottoMainWindow();
            } catch (Exception er) {
                ErrorStackPrint errorOpenGui = new ErrorStackPrint(er, "Gui Error", "Failed to open the Lotto Window", "Sorry but an error occured. Please try to restart the Application or report this issue.");
                Alert errorOPenGuiAlert = errorStackPrint.createErrorWindow();
                errorOPenGuiAlert.showAndWait();
                Platform.exit();
            }

            /*####################*/
            /*END OF REMOVE LINES*/
            /*###################*/
        }
    }

    public void openLottoMainWindow() throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("lottoMain.fxml"));
        Parent root = loader.load();
        LottoMainController controller = loader.getController();
        controller.setClient(this.client);

        Stage lottoStage = new Stage();
        lottoStage.getIcons().add(new Image(ErrorStackPrint.class.getResourceAsStream("assets/logo/lotto_logo.png")));
        lottoStage.setTitle("Lotto - " + this.username);
        Scene mainScene = new Scene(root, 500, 600);

        /*
        * @comment Note that perhaps the css file isnt correct and has to be adjusted
        * */
        mainScene.getStylesheets().add(getClass().getResource("../assets/css/lotto.min.css").toExternalForm());
        lottoStage.setScene(mainScene);

        StackPane[] buttons = new StackPane[49];
        for(int i=0;i<49;i++){
            StackPane stackPane = new StackPane();
            Button button = new Button(Integer.toString(i+1));
            Line line1 = new Line();
            Line line2 = new Line();
            //Style classes for the Number button
            stackPane.getStyleClass().add("numberChoosePane");
            button.getStyleClass().add("numberButton");
            button.prefHeightProperty().bind(stackPane.heightProperty());
            button.prefWidthProperty().bind(stackPane.widthProperty());

            line1.endYProperty().bind(stackPane.heightProperty());
            line1.setVisible(false);
            line1.setRotate(45);
            line1.setMouseTransparent(true);
            line2.endYProperty().bind(stackPane.heightProperty());
            line2.setVisible(false);
            line2.setRotate(-45);
            line2.setMouseTransparent(true);

            line1.getStyleClass().add("numberButtonLine");
            line2.getStyleClass().add("numberButtonLine");

            line1.setId("line1");
            line2.setId("line2");

            //Add the NOdes to the StackPane
            stackPane.getChildren().addAll(button, line1, line2);
            buttons[i] = stackPane;
        }
        controller.addLottoZahlen(buttons);

        lottoStage.setOnCloseRequest(e -> {
            Platform.exit();
        });

        lottoStage.show();
    }
}
