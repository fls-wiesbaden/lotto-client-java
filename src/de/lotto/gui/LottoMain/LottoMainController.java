package de.lotto.gui.LottoMain;

import de.lotto.Client.Client;
import de.lotto.gui.ErrorStackPrint;
import de.lotto.gui.Login.LoginView;
import de.lotto.gui.LottoMain.balance.BalanceView;
import de.lotto.gui.LottoMain.history.HistoryView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class LottoMainController {

    private static final int INC = 1;
    private static final int DEC = -1;

    private Scene mainScene;


    @FXML
    private BorderPane lottoMainStage;

    @FXML
    private VBox vBoxGridWrapper;

    @FXML
    private Label numsToChoose;

    @FXML
    private MenuItem menuHistory;

    @FXML
    private MenuItem menuBalance;

    private Client client;

    private int[] selected = new int[6];
    private int choosen = 0;

    @FXML
    public void initialize(){
        this.menuHistory.setAccelerator(KeyCombination.keyCombination("Ctrl+H"));
        this.menuBalance.setAccelerator(KeyCombination.keyCombination("CTRL+B"));
    }

    void setClient(Client client){
        this.client = client;
    }

    @FXML
    private void onAddNum(MouseEvent e){
        for(int i=0;i<this.selected.length;i++){
            if(this.selected[i] == 0){
                Button clicked = (Button) e.getSource();
                StackPane stackPane = (StackPane) clicked.getParent();
                stackPane.getStyleClass().add("active");
                clicked.setOnMouseClicked(event -> this.onRemNum(event));

                for (Node child : stackPane.getChildren()) {
                    if(child instanceof Line){
                        Line line = (Line) child;
                        line.setVisible(true);
                    }
                }

                this.selected[i] = Integer.parseUnsignedInt(clicked.getText());
                this.updateToChooseLabel(LottoMainController.INC);
                return;
            }
        }

    }

    @FXML
    private void onRemNum(MouseEvent e){
        Button clicked = (Button) e.getSource();

        StackPane stackPane = (StackPane) clicked.getParent();
        stackPane.getStyleClass().remove("active");
        clicked.setOnMouseClicked(event -> this.onAddNum(event));

        for (Node child : stackPane.getChildren()) {
            if(child instanceof Line){
                child.setVisible(false);
                //((Line) child).setEndY(0);
            }
        }

        int delVal = Integer.parseUnsignedInt(clicked.getText());
        for(int i=0;i<this.selected.length;i++){
            if(this.selected[i] == delVal){
                this.selected[i] = 0;
                this.updateToChooseLabel(LottoMainController.DEC);
                return;
            }
        }
    }

    @FXML
    private void clickLogOutMenu(){
        Platform.exit();
    }

    @FXML
    private void openHistroy() {
        mainScene = this.lottoMainStage.getScene();

        HistoryView historyView = new HistoryView(mainScene);
        Scene historyScene = historyView.getScene();

       Stage stage = (Stage) this.lottoMainStage.getScene().getWindow();
       try{
           stage.setScene(historyScene);
       }catch(Exception e){
           Alert alert = new ErrorStackPrint(e, "History error", "Error showing history", "Sorry but an error occured. Please try to restart the Application or report this issue.").createErrorWindow();
           alert.showAndWait();
       }
    }

    @FXML
    private void openBalance() {
        mainScene = this.lottoMainStage.getScene();

        BalanceView balanceView = new BalanceView(mainScene);
        Scene balanceScene = balanceView.getScene();

        Stage stage = (Stage) this.lottoMainStage.getScene().getWindow();
        stage.setScene(balanceScene);
    }

    void updateToChooseLabel(int opt){

        switch (opt){
            case LottoMainController.INC:
                this.choosen++;
                break;
            case LottoMainController.DEC:
                this.choosen--;
                break;
        }

        if(this.choosen == this.selected.length){
            this.numsToChoose.getStyleClass().add("green");
        }else{
            this.numsToChoose.getStyleClass().remove("green");
        }

        this.numsToChoose.setText(Integer.toString(this.selected.length - this.choosen));
    }

    @FXML
    public void submitLottoNumbers(){
        boolean hasError = false;


        for(int select : this.selected){
            if(select == 0){
                hasError = true;
            }
        }

        if(!hasError){
            try{
                this.client.sendNumbers(this.selected);
                //Send if successfully send the numbers to the server
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Numbers submitted to the Lotto server");
                alert.setHeaderText(null);
                alert.setContentText("Nice! Your numbers were successfully sent.");
                Stage alertStage = (Stage) alert.getDialogPane().getScene().getWindow();
                alertStage.getIcons().add(new Image(getClass().getResourceAsStream("../assets/logo/lotto_logo.png")));
                alert.initOwner(this.lottoMainStage.getScene().getWindow());

                alert.showAndWait();
            }catch(Exception e){
                e.printStackTrace();
            }

            for(int i=0;i<this.selected.length;i++){
                this.selected[i] = 0;
                updateToChooseLabel(LottoMainController.DEC);
            }

            GridPane gridPane = (GridPane) vBoxGridWrapper.getChildren().get(0);
            gridPane.getChildren().forEach(node -> {
                if(node instanceof StackPane){
                    if(node.getStyleClass().contains("active")){
                        node.getStyleClass().remove("active");

                        ((StackPane) node).getChildren().forEach(node1 -> {
                            if(node1 instanceof Line){
                                Line line = (Line) node1;
                                line.setVisible(false);
                            }
                        });
                    }
                }
            });
        }
    }

    @FXML
    public void genQuickTip(){
        GridPane gridPane = (GridPane) this.vBoxGridWrapper.getChildren().get(0);
        int[] allNums = new int[this.selected.length];
        this.clearAllNumbers();

        for(int i=0;i<this.selected.length;i++) {
            int num = (int) Math.floor(Math.random() * 49) + 1;

            for(int j=0;j<allNums.length;j++){
                if(num == allNums[j]){
                    num = (int) Math.floor(Math.random() * 49) + 1;
                }
            }
            allNums[i] = num;
            if(this.selected[i] == 0){
                this.selected[i] = num;
                this.updateToChooseLabel(LottoMainController.INC);
                for (Node node : gridPane.getChildren()) {
                    StackPane stackPane = (StackPane) node;
                    for(Node child : stackPane.getChildren()){
                        if(child instanceof Button && ((Button) child).getText().equals(Integer.toString(num))){
                            stackPane.getStyleClass().add("active");
                            child.setOnMouseClicked(event -> this.onRemNum(event));

                            for (Node line : stackPane.getChildren()) {
                                if(line instanceof Line){
                                    Line l = (Line) line;
                                    l.setVisible(true);
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    @FXML
    private void clearAllNumbers(){
        GridPane gridPane = (GridPane) this.vBoxGridWrapper.getChildren().get(0);

        for(Node stackPaneNode : gridPane.getChildren()){
            if(stackPaneNode instanceof StackPane){
                StackPane stackPane = (StackPane) stackPaneNode;

                for(Node childNode : stackPane.getChildren()){
                    if(childNode instanceof Button){
                        Button btn = (Button) childNode;
                        int btnVal = Integer.parseUnsignedInt(btn.getText());
                        for(int i=0; i < this.selected.length;i++){
                            if(btnVal == this.selected[i]){
                                btn.getParent().getStyleClass().remove("active");
                                btn.setOnMouseClicked(event -> this.onAddNum(event));
                                this.updateToChooseLabel(LottoMainController.DEC);
                                this.selected[i] = 0;
                                for(Node lineNode : stackPane.getChildren()){
                                    if(lineNode instanceof Line){
                                        Line line = (Line) lineNode;
                                        line.setVisible(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void hoverNumberButton(MouseEvent event){
        Button button = (Button) event.getSource();
        StackPane stackPane = (StackPane) button.getParent();
        stackPane.getStyleClass().add("hover");
        stackPane.setScaleX(1.1);
        stackPane.setScaleY(1.1);
    }

    void unHoverNumberButton(MouseEvent event){
        Button button = (Button) event.getSource();
        StackPane stackPane = (StackPane) button.getParent();
        stackPane.getStyleClass().remove("hover");
        stackPane.setScaleX(1);
        stackPane.setScaleY(1);
    }

     void addLottoZahlen(StackPane[] stackPanes){
        GridPane numberGrid = new GridPane();
        numberGrid.setId("numberButton");
        numberGrid.getStyleClass().add("numberGrid");

        int j = 0;
        for(int i=0;i<49;i++){
            if(j > 6) j = 0;
            stackPanes[i].getChildren().get(0).setOnMouseClicked(e -> this.onAddNum(e));
            stackPanes[i].getChildren().get(0).setOnMouseEntered(e -> this.hoverNumberButton(e));
            stackPanes[i].getChildren().get(0).setOnMouseExited(e -> this.unHoverNumberButton(e));
            numberGrid.add(stackPanes[i], j,i / 7);
            j++;
        }

        vBoxGridWrapper.getChildren().add(0, numberGrid);
    }

}
