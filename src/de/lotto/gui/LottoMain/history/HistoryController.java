package de.lotto.gui.LottoMain.history;

import de.lotto.Client.WrapVariables;
import de.lotto.database.MySqlConnection;
import de.lotto.models.Tip;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class HistoryController {

    private Scene lottoScene;

    @FXML
    private VBox historyStage;

    @FXML
    private ListView<Pane> dbData;

    private ArrayList<Tip> userTips = new ArrayList<>();

    private ObservableList<Pane> listViewItems = FXCollections.observableArrayList();

    public void setLottoScene(Scene lottoScene) {
        this.lottoScene = lottoScene;
    }

    @FXML
    public void goBack(){
        Stage stage = (Stage) this.historyStage.getScene().getWindow();
        stage.setScene(this.lottoScene);
    }

    public void fetchTips(){

        MySqlConnection mySqlConnection = new MySqlConnection();

        try{
            PreparedStatement statement = mySqlConnection.connect().prepareStatement("SELECT * FROM user_tip WHERE user_id=?");
            //PreparedStatement statement = mySqlConnection.connect().prepareStatement("SELECT * FROM user_tip WHERE user_id=14");
            statement.setInt(1, WrapVariables.user.getUser_id());

            System.out.println(WrapVariables.user.getUser_id());

            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()) {
                int user_id = resultSet.getInt("user_id");
                int tip_id = resultSet.getInt("tip_id");
                String tipData = resultSet.getString("tip");
                boolean pending = !(resultSet.getInt("pending") == 1);
                Date date = resultSet.getDate("tip_date");

                Tip tip = new Tip(user_id, tip_id, tipData, pending, date);
                this.userTips.add(tip);
            }

        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            mySqlConnection.disconnect();
        }

    }

    public void setTipData() {
        Platform.runLater(() -> {
            fetchTips();
        });

        VBox pane = new VBox();

        for (Tip tip : this.userTips) {

            //Label for tips data
            Label tipNums = new Label(tip.getTip());

            //pending state
            HBox tipState = new HBox();
            Label stateLabel = new Label("Vorbei: ");
            Label stateLabel1 = new Label(tip.getPending() ? "Ja" : "Nein");
            stateLabel1.getStyleClass().add("tipState");
            tipState.getChildren().addAll(stateLabel, stateLabel1);

            //gewinn state
            VBox vBox1 = new VBox();
            if(tip.getPending()){
                int richtige = calcRichtige(tip.getId());
                Label gewinn = new Label("Richtige / Gewinn: ");
                Label gewinnNum = new Label(richtige + " / " + calcGewinn(richtige));

                HBox vBox = new HBox();
                vBox.getChildren().addAll(gewinn, gewinnNum);
                vBox1.getChildren().addAll(vBox, tipState);
            }else{
                vBox1.getChildren().addAll(tipState);
            }



            //wrapp it up
            HBox hBox = new HBox();
            hBox.getChildren().addAll(tipNums, vBox1);
            this.listViewItems.add(hBox);


        }

        this.dbData.setItems(this.listViewItems);
    }

    private double calcGewinn(int richtige){

        double gewinn = 0;

        switch (richtige){
            case 2:
                gewinn = 3;
                break;
            case 3:
                gewinn = 10;
                break;
            case 4:
                gewinn = 500;
                break;
            case 5:
                gewinn = 10000;
                break;
            case 6:
                gewinn = 1000000;
        }

        return gewinn;
    }

    private int calcRichtige(int id){

        MySqlConnection mySqlConnection = new MySqlConnection();
        int richtige = 0;

        try{
            PreparedStatement statement = mySqlConnection.connect().prepareStatement("SELECT ziehungen_id, tip FROM user_tip WHERE user_id=? AND tip_id=? AND ziehungen_id IS NOT NULL");
            statement.setInt(1, WrapVariables.user.getUser_id());
            statement.setInt(2, id);
            ResultSet rs = statement.executeQuery();

            if(rs.first()){
                PreparedStatement statement1 = mySqlConnection.connect().prepareStatement("SELECT * FROM ziehungen_id WHERE ziehungen_id=?");
                statement1.setInt(1, rs.getInt("ziehungen_id"));
                ResultSet resultSet = statement1.executeQuery();

                if(resultSet.first()){
                    String[] tipData = rs.getString("tip").split(";");
                    System.out.println(tipData);
                    int[] tipArray = new int[tipData.length];
                    for (int i=0;i<tipArray.length;i++) {
                        tipArray[i] = Integer.parseUnsignedInt(tipData[i]);
                    }
                    String[] ziehungData = resultSet.getString("zahlen").split(";");
                    System.out.println(ziehungData);
                    int[] ziehungenArray = new int[ziehungData.length];
                    for (int i=0;i<ziehungenArray.length;i++) {
                        ziehungenArray[i] = Integer.parseUnsignedInt(ziehungData[i]);
                    }

                    for(int i=0;i<tipArray.length;i++){
                        for(int j=0;j<ziehungenArray.length;j++){
                            if(tipArray[i] == ziehungenArray[j]){
                                richtige++;
                            }
                        }
                    }
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally{
            mySqlConnection.disconnect();
        }

        return richtige;
    }
}
