package de.lotto.gui.LottoMain.history;

import de.lotto.gui.LottoMain.LottoMainController;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class HistoryView {

    private Scene lottoScene;

    public HistoryView(Scene lottoScene){
    this.lottoScene = lottoScene;
    }

    public Scene getScene(){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("history.fxml"));
            Parent root = loader.load();
            HistoryController controller = loader.getController();
            controller.setLottoScene(this.lottoScene);
            controller.setTipData();
            Scene mainScene = new Scene(root, 500, 600);

            /*
             * @comment Note that perhaps the css file isnt correct and has to be adjusted
             * */
            mainScene.getStylesheets().add(getClass().getResource("../../assets/css/lotto.min.css").toExternalForm());

            return mainScene;
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

}
