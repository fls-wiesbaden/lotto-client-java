package de.lotto.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySqlConnection {

    //private static final String DATABASE_URL = "jdbc:mysql://10.5.253.171/lotto";
    private static final String DATABASE_URL = "jdbc:mysql://localhost/lotto";
    private static final String USERNAME = "joel";
    private static final String PASSWORD = "12345678joel";
    private static final String DATABASE_TIMEZONE = "EST5EDT";
    //private static final String MAX_POOL = "250";

    private Connection connection;
    private Properties properties;

    public MySqlConnection(){
        this.connection = null;
        this.properties = null;
    }

    private Properties getProperties() {
        if(this.properties == null){
            this.properties = new Properties();
            this.properties.setProperty("user", USERNAME);
            this.properties.setProperty("password", PASSWORD);
            this.properties.setProperty("serverTimezone", DATABASE_TIMEZONE);
        }

        return this.properties;
    }

    public Connection connect() {
        if(connection == null){
            try{
                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return connection;
    }

    public void disconnect() {
        if(this.connection != null) {
            try {
                this.connection.close();
                connection = null;
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

}
