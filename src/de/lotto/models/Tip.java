package de.lotto.models;

import java.util.Date;

public class Tip {

    private int user_id;
    private  int id;
    private String tip;
    private boolean pending;
    private Date date;

    public Tip(int user_id, int id, String tip, boolean pending, Date date){
        this.user_id = user_id;
        this.id = id;
        this.tip = tip;
        this.pending = pending;
        this.date = date;
    }

    public boolean getPending() {
        return pending;
    }

    public Date getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getTip() {
        return tip;
    }
}
