package de.lotto.models;

import java.math.BigDecimal;
import java.math.BigInteger;

public class User {

    private String username;
    private int user_id;
    private BigDecimal balance;

    public User(String username, int user_id, BigDecimal balance){
        this.user_id = user_id;
        this.username = username;
        this.balance = balance;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getUsername() {
        return username;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
