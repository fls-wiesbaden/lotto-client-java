package de.lotto.Client;

import de.lotto.models.User;

import java.math.BigDecimal;

public class WrapVariables {

    public static User user = new User("No user data", 0, new BigDecimal(0));

    public static void setUser(User user) {
        WrapVariables.user = user;
    }
}
