package de.lotto.Client;

import de.lotto.database.MySqlConnection;
import de.lotto.models.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Client {

    private final int PORT;
    private final String HOST;
    private BufferedReader bufferedReader;
    private boolean isConnected;
    private User user;
    private String username;
    private Socket client;

    public Client(int port, String host, String username) throws IOException{
        this.HOST = host;
        this.PORT = port;
        this.isConnected = false;
        this.username = username;
        this.client  = new Socket(this.HOST, this.PORT);
    }

    public User fetchUser(int id){
        User user = null;

        MySqlConnection mySqlConnection = new MySqlConnection();

        try{
            PreparedStatement statement = mySqlConnection.connect().prepareStatement("SELECT * FROM user WHERE user_id=?");
            statement.setInt(1, id);


            ResultSet resultSet = statement.executeQuery();

            int rowCount = 0;

            if(resultSet.last()){
                rowCount = resultSet.getRow();
                resultSet.beforeFirst();
            }

            if(rowCount > 0){
                if(resultSet.first()){
                    user = new User(resultSet.getString("username"), resultSet.getInt("user_id"), resultSet.getBigDecimal("balance"));
                }
            }else{
                System.out.println("Create user: " + username);
                PreparedStatement psCU = mySqlConnection.connect().prepareStatement("INSERT INTO user (username) VALUES (?)");
                psCU.setString(1, username);

                psCU.executeUpdate();
            }


        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            mySqlConnection.disconnect();
        }

        return user;

    }

    public void verbinden() throws NullPointerException{
        if(this.client.isConnected()){

            this.isConnected = true;
            System.out.println("Client verbunden");
            this.sendText(username);

           try{
               while(client.getInputStream().available() < 0);
               BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
               String userId = reader.readLine();
               user = fetchUser(Integer.parseInt(userId));

               WrapVariables.setUser(user);

               System.out.println(user.getUser_id());
               System.out.println(user.getUsername());
           }catch(Exception e){
               e.printStackTrace();
           }
        }else{
            this.isConnected = false;
            System.out.println("Client nicht vebunden");
        }
    }

    public void sendNumbers(int[] numbers){
        for(int i=0;i<numbers.length;i++){
            this.sendNumberInt(numbers[i]);
            try{
                Thread.sleep(200);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public void sendText(String text){
        try{
            PrintWriter printWriter = new PrintWriter(this.client.getOutputStream());
            printWriter.println(text);
            printWriter.flush();
        }catch(IOException ioe){
            System.out.println("Konnte den Text: " + text + " aufgrund eines Fehlers nicht senden");
        }
    }

    public void sendNumberDouble(double num){
        try{
            PrintWriter printWriter = new PrintWriter(this.client.getOutputStream());
            printWriter.println(num);
            printWriter.flush();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

    public void sendNumberInt(int num){
        try{
            PrintWriter printWriter = new PrintWriter(this.client.getOutputStream());
            printWriter.println(num);
            printWriter.flush();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

}

